# git cheat sheet

Read your group instruction in the text files 

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)

$ git diff
zeigt �nderungen zw. getrackten datei und  �nderungen, die noch nicht gestaged wurden
$ git diff --staged
�nderungen im stage zustande, aber noch nicht comitted
$ git diff test.txt
�nderungen nur f�r datei test.txt, die noch nicht gestaged wurden
$ git diff --theirs
bei einem merge werden die �nderungen des remote/origin/master angezeigt??
$ git diff --ours
bei einem merge werden die �nderungen des lokalen gits angezeigt??
$ git log
zeigt alle commits an
$ git log --oneline
zeigt die commits in einer zeile an
$ git log --oneline --all
zus�tzliche anzeige der remote commits
$ git log --oneline --all --graph
zus�tzlich anzeige mit baum
$ git log --oneline --all --graph --decorate
kein unterschied erkennbar
$ git log --follow -p -- filename
erkennt, dass datei umbenannt wurde und zeigt zus�tzlich die �nderungen unter altem dateinamen an
$ git log -S'static void Main'
sucht in der historie des codes / der comitteten deltas nach static void Main 
$ git log --pretty=format:"%h - %an, %ar : %s"
ausf�hrlichere infos zum commit: autor, zeitangabe in einer zeile

[group4.md](group4.md)

and place your solution in a fitting category in this file. It's best to create a branch while working on the exercise!

## Creating repositories

Der wird dir nicht gefallen.

## Staging and committing

... put commands and explanation here

## Inspecting the repository and history

... put commands and explanation here

## Managing branches

$ git checkout work     Switch to branch named "work" or restore working tree files from the branch
$ git checkout master       Switch to branch named "master" or restore working tree files from the branch
$ git branch feature1       erstellt einen Branch mit dem Namen "feature1"
$ git checkout -b work      erstellt einen Branch mit dem Namen work und checkt diesen direkt aus
$ git checkout work         setzt den Head auf den aktuellen commit vom Branch "work"
$ git branch                listet alle lokalen Branches auf
$ git branch -v             listet die lokalen Branches mit Infos über den letzten commit auf
$ git branch -a             listet alle Branches (inkl. remote Branches) auf
$ git branch --merged       listet nur die bereits gemergeden Branches auf
$ git branch --no-merged    listet nur die Branches auf, die noch nicht gemerged sind
$ git branch -d work        löscht Branch "work" (nur im cleanstate)
$ git branch -D work        löscht Branch "work" (auch im dirtystate)

## Merging

.. put commands and examples here
